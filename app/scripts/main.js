var $image = $('#image');
var data = [];
var tData = {};

$image.cropper({
  aspectRatio: NaN,
  zoomable: false,
  sizable: false,
  movable: false,
  sacalable: false,
  viewMode: 1,
  data : {
  	x : 0,
  	y: 0,
  	width: 0,
  	height: 0
  },
  crop: function(event) {

  	tData = {
  		x : event.detail.x,
  		y : event.detail.y,
  		height : event.detail.height,
  		width : event.detail.width,
  	};

  }
});

$('#addPoint').click(function() {
	data.push({
		name : $('#name').val(),
		data : tData
	});

	$('#reset').click();
	renderPoints();
	console.log(data);
});
	

$(document).on('click', '.item', function() {
	var index = $(this).attr('data-index');
	var d = data[index];
	
	$image.cropper('setData', {
		x : d.data.x,
		y : d.data.y,
		height : d.data.height,
		width : d.data.width,
	});

	$image.cropper('crop');

});

$('#reset').click(function() {
	$image.cropper('setData', {
		x : 0,
		y: 0,
		width: 0,
		height: 0
	});

	$image.cropper('crop');
});

$('#generate').click(function() {
	for(var d of data) {
		$image.cropper('setData', {
			x : d.data.x,
			y : d.data.y,
			height : d.data.height,
			width : d.data.width,
		});

		 $image.cropper('crop');

		//console.log($image.cropper('getCroppedCanvas', {}).toDataUrl());
		var canvas = $image.cropper('getCroppedCanvas', {});
		
		Tesseract.recognize(canvas.toDataURL())
		.then(function(result){
		    $('#generated').append('Field : ' + d.name + '<br/> Confidence : ' + result.confidence + '<br/> Content : ' + result.text + '<hr>');
		   
		});
	}

	restart();
});

$('#restart').click(function() {
	$('#generated').html('');
});

function restart() {
	data = [];
	tData = {};
	$('#reset').click();
	renderPoints();
}

function renderPoints() {
	$('#dataPoints').html('');
	var x = 0;
	for(var d of data) {
		$('#dataPoints').append(`
			<li class="item" data-index="`+ x +'">'+ d.name +`</li>
		`);

		x++;
	}
}